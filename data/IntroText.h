static const char *introTextData =
	"\brBrilliant \x0EMinds \blOf The \bbNavy - \blVersion " MTOS(MOD_SAVE_VERSION) "." MTOS(MOD_MINOR_VERSION) " - http://brilliant-minds.tk, chat.freenode.net #BMN\n"
	"\x7F\x7F\x7F\x7F\x7F\x7F\x7F\x7F\x7F\x7F\x7F\x7F\x7F\x7F\x7F\x7F\x7F\x7F\x7F\n"
	"\blBased On Version " MTOS(SAVE_VERSION) "." MTOS(MINOR_VERSION) " - http://powdertoy.co.uk\n"
	"\n"
	"\bgControl+C/V/X are Copy, Paste and cut respectively.\n"
	"\bgTo choose a material, hover over one of the icons on the right, it will show a selection of elements in that group.\n"
	"\bgPick your material from the menu using mouse left/right buttons.\n"
	"Draw freeform lines by dragging your mouse left/right button across the drawing area.\n"
	"Shift+drag will create straight lines of particles.\n"
	"Ctrl+drag will result in filled rectangles.\n"
	"Ctrl+Shift+click will flood-fill a closed area.\n"
	"Use the mouse scroll wheel, or '[' and ']', to change the tool size for particles.\n"
	"Middle click or Alt+Click to \"sample\" the particles.\n"
	"Ctrl+Z will act as Undo.\n"
	"\n\boUse 'Z' for a zoom tool. Click to make the drawable zoom window stay around. Use the wheel to change the zoom strength.\n"
	"The spacebar can be used to pause physics. Use 'F' to step ahead by one frame.\n"
	"Use 'S' to save parts of the window as 'stamps'. 'L' loads the most recent stamp, 'K' shows a library of stamps you saved.\n"
	"Use 'P' to take a screenshot and save it into the current directory.\n"
	"Use 'H' to toggle the HUD. Use 'D' to toggle debug mode in the HUD.\n"
	"\n"
	"Contributors: \bgStanislaw K Skowronek (Designed the original Powder Toy),\n"
	"\bgSimon Robertshaw, Skresanov Savely, cracker64, Catelite, Bryan Hoyle, Nathan Cousins, jacksonmj,\n"
	"\bgFelix Wallin, Lieuwe Mosch, Anthony Boot, Matthew \"me4502\", MaksProg, jacob1, mniip\n"
	"\n"
#ifndef BETA
	"\bgThis is a MOD, you cannot save things publicly. If you are planning on publishing any saves, please use TPT\n"
#else	
	"\brThis is a BETA, use at your own risk, report any bugs on GitHub. You cannot save things publicly.\n" 
	"\brIf you are planning on publishing any saves, please use TPT\n"
#endif
	"\n"
#ifdef SNAPSHOT
	"SNAPSHOT " MTOS(SNAPSHOT_ID) " "
#endif
	"\bt" MTOS(MOD_SAVE_VERSION) "." MTOS(MOD_MINOR_VERSION) "." MTOS(MOD_BUILD_NUM) " " IDENT_PLATFORM " "
#ifdef X86
	"X86 "
#endif
#ifdef X86_SSE
	"X86_SSE "
#endif
#ifdef X86_SSE2
	"X86_SSE2 "
#endif
#ifdef X86_SSE3
	"X86_SSE3 "
#endif
#ifdef X64
	"X64 "
#endif
#ifdef X64_SSE
	"X64_SSE "
#endif
#ifdef X64_SSE2
	"X64_SSE2 "
#endif
#ifdef X64_SSE3
	"X64_SSE3 "
#endif
#ifdef LUACONSOLE
	"LUACONSOLE "
#endif
#ifdef GRAVFFT
	"GRAVFFT "
#endif
#ifdef REALISTIC
	"REALISTIC"
#endif
	;
